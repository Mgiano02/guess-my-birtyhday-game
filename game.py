# from random import randint

# random_month = randint(1,12)

# random_year = randint(1924,2004)

# name = input("Hi! What is your name?")

# guess_birthday = input(name,"were you born in ", random_month, " / ", random_year, "?")

# if guess_birthday == "Yes":
#     print("I knew")
# elif guess_birthday == "No":
#     print("Drat! Lemme try again!")
# -----

from random import randint
name = input("Hi! What is your name? ")

for guess_number in range(1,6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess", guess_number,":", name,", were you born in", month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    else:
        print("Drat! Lemme try again!")
